import { IDataProvider } from './IDataProvider';
import { jsonList } from '../json-mocks/json-list';
import { ListItem } from '../../models/items';
import { of, BehaviorSubject } from 'rxjs';

export class MockedDataProvider implements IDataProvider {
    private biggestId = 0;
    private items: ListItem[] = [...jsonList];
    private words: String[] = [
        'macka', 'znacka', 'nemacka', 'pljacka', 'this',
        'that', 'that', 'bird', 'clock', 'boy', 'plastic', 'duck', 'teacher', 'old lady', 'professor', 'hamster', 'dog',
        'kicked', 'ran', 'flew', 'dodged', 'sliced', 'rolled', 'died', 'breathed', 'slept', 'killed',
        'beautiful', 'lazy', 'professional', 'lovely', 'dumb', 'rough', 'soft', 'hot', 'vibrating', 'slimy',
        'slowly', 'elegantly', 'precisely', 'quickly', 'sadly', 'humbly', 'proudly', 'shockingly', 'calmly', 'passionately',
        'down', 'into', 'up', 'on', 'upon', 'below', 'above', 'through', 'across', 'towards'];
    private itemsListSubject = new BehaviorSubject<any>(this.items);

    getListItems() {
        return this.itemsListSubject.asObservable();
    }

    updateListItem(id) {
        let itemsUpdated: ListItem[] = [];
        itemsUpdated = this.items.map((item, i) => {
            itemsUpdated.push(item);
            if (item.id === id) {
                item.isChecked = !item.isChecked;
            }
            return item;
        });
        this.items = itemsUpdated;
        this.itemsListSubject.next(this.items);
        return of(itemsUpdated);
    }

    addGeneratedItem(description: string) {
        const newItem: ListItem = {
            id: this.getBiggestId(),
            description: this.createSentence(description),
            isChecked: false
        };
        if (newItem.description === '') {
            return of([new ListItem()]);
        } else {
            this.items.push(newItem);
            this.itemsListSubject.next(this.items);
            return of(this.items);
        }
    }

    getBiggestId() {
        this.items.forEach(element => {
            if (this.biggestId < element.id) {
                this.biggestId = element.id;
            }
        });
        this.biggestId += 1;
        return this.biggestId;
    }

    randGen() {
        return Math.floor(Math.random() * this.words.length);
    }

    createSentence(inputDescription) {
        const rand1 = Math.floor(Math.random() * 20) + 1;
        let description = '';
        if (inputDescription) {
            const z = this.items.filter(element => {
                if (element.description === inputDescription) {
                    return true;
                }
            });
            if (z.length) {
                description = '';
            } else {
                description = inputDescription;
            }
        } else {
            description = 'The';
            for (let i = 0; i < rand1; i++) {
                description = description + ' ' + this.words[this.randGen()];
            }
        }
        return description;
    }
}
