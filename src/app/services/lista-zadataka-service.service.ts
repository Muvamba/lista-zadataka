import { Injectable } from '@angular/core';
import { IDataProvider } from 'src/app/services/IDataProvider';
import { MockedDataProvider } from 'src/app/services/MockedDataProvider';

@Injectable()
export class ListaZadatakaServiceService {
  private dataProvider: IDataProvider;
  constructor() {
    this.dataProvider = new MockedDataProvider();
  }
  public getListItems() {
    return this.dataProvider.getListItems();
  }
  public updateListItem(id) {
    return this.dataProvider.updateListItem(id);
  }
  public addGeneratedItem(description) {
    return this.dataProvider.addGeneratedItem(description);

  }
}
