import { Observable } from 'rxjs';
import { ListItem } from 'src/models/items';

export interface IDataProvider {
    getListItems(): Observable<ListItem[]>;
    updateListItem(id: number): Observable<ListItem[]>;
    addGeneratedItem(description: string): Observable<ListItem[]>;
}
