import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderServiceService {
  private shouldShow = true;
  private showLoaderSubject = new BehaviorSubject<any>(this.shouldShow);

  constructor() {}

  shouldShowLoader() {
    return this.showLoaderSubject.asObservable();
  }

  updateShowLoader(booleance) {
    this.showLoaderSubject.next(booleance);
  }

}
