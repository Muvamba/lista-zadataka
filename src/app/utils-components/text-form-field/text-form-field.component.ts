import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ListaZadatakaServiceService } from '../../services/lista-zadataka-service.service';
import { LoaderServiceService } from 'src/app/services/loader-service.service';
import { ToasterService, Toast, ToasterConfig } from 'angular2-toaster';

@Component({
  selector: 'app-text-form-field',
  templateUrl: './text-form-field.component.html',
  styleUrls: ['./text-form-field.component.scss']
})
export class TextFormFieldComponent {
  description = new FormControl('', [Validators.required, Validators.minLength(3)]);
  public config1: ToasterConfig = new ToasterConfig({
    positionClass: 'toast-top-right',
    animation: 'fade'
  });

  constructor(private listaService: ListaZadatakaServiceService, private loader: LoaderServiceService,
    private toasterService: ToasterService) { }

  onAdd() {
    if (this.description.value !== '') {
      this.loader.updateShowLoader(true);

      this.listaService.addGeneratedItem(this.description.value).subscribe(
        listItems => {
          if (listItems[0].id === 0) {
            const toast: Toast = {
              type: 'error',
              title: 'Greska',
              body: 'Zadatak sa ovim imenom vec postoji.',
              showCloseButton: true
            };
            this.toasterService.pop(toast);
          } else {
            const toast: Toast = {
              type: 'success',
              title: 'Bravo!!!',
              body: 'Zadatak uspesno dodat.',
              showCloseButton: true
            };
            this.toasterService.pop(toast);
            this.description.reset();
          }
          this.loader.updateShowLoader(false);
        }
      );
    }
  }

  getErrorMessage() {
    return this.description.hasError('required') ? 'You must enter a value' :
      this.description.hasError('description') ? 'Not a valid length' :
        '';
  }
}
