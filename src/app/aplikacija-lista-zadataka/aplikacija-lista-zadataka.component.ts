import { Component, OnInit } from '@angular/core';
import { LoaderServiceService } from '../services/loader-service.service';

@Component({
  selector: 'app-aplikacija-lista-zadataka',
  templateUrl: './aplikacija-lista-zadataka.component.html',
  styleUrls: ['./aplikacija-lista-zadataka.component.scss']
})
export class AplikacijaListaZadatakaComponent implements OnInit {
  private reseniNereseniObjekat: {};
  private showLoaderClass = 'zadatak-class-spinner';

  constructor(private loader: LoaderServiceService) { }

  ngOnInit() {
    this.shouldShowLoader();
  }

  emitForChips(reseniNereseniObjekat) {
    this.reseniNereseniObjekat = reseniNereseniObjekat;
  }

  shouldShowLoader() {
    this.loader.shouldShowLoader().subscribe(
      showItem => {
        if (showItem) {
          this.showLoaderClass = 'zadatak-class-spinner';
        } else {
          this.showLoaderClass = 'zadatak-class-spinner-hide';
        }
      });
  }
}
