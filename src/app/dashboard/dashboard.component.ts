import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ListaZadatakaServiceService } from '../services/lista-zadataka-service.service';
import { LoaderServiceService } from '../services/loader-service.service';
import { ToasterService, ToasterConfig, Toast } from 'angular2-toaster';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  private isGeneratorRunning = false;
  private geratorIneterval;
  private generateButtonValue = 'Generisi zadatke';
  private generateButtonClass = 'generate-button-class';
  @Output() emitForChips = new EventEmitter();

  public config1: ToasterConfig = new ToasterConfig({
    positionClass: 'toast-top-right',
    animation: 'fade'
  });

  constructor(private listaService: ListaZadatakaServiceService, private loader: LoaderServiceService,
    private toasterService: ToasterService) {
  }

  ngOnInit() {
    this.pokreniGenerator();
  }

  _emitForChips(reseniNereseniObjekat) {
    this.emitForChips.emit(reseniNereseniObjekat);
  }

  pokreniGenerator() {
    const toast: Toast = {
      type: 'info',
      title: 'Generisanje pokrenuto',
      body: 'Na svakih 20 sekundi generisemo vam po zadatak.',
      showCloseButton: true
    };
    this.toasterService.pop(toast);

    if (!this.isGeneratorRunning) {
      this.isGeneratorRunning = !this.isGeneratorRunning;
      this.generateButtonValue = 'Zaustavi generator';
      this.generateButtonClass = 'generate-button-class-stop';
      this.geratorIneterval = setInterval(
        () => {
          this.kreniGeneratoru();
        }, 20000);
    } else {
      this.staniGeneratoru();
    }
  }

  kreniGeneratoru() {
    this.listaService.addGeneratedItem('').subscribe(
      () => {
        this.loader.updateShowLoader(false);
      });
  }

  staniGeneratoru() {
    const toast: Toast = {
      type: 'info',
      title: 'Generisanje zaustavljeno',
      body: 'Zaustavili ste generator zadataka',
      showCloseButton: true
    };

    clearInterval(this.geratorIneterval);
    this.isGeneratorRunning = !this.isGeneratorRunning;
    this.generateButtonValue = 'Generisi zadatke';
    this.generateButtonClass = 'generate-button-class';
  }

}
