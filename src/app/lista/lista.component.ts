import { Component, EventEmitter, Output, Input, OnChanges, OnInit } from '@angular/core';
import { ListaZadatakaServiceService } from '../services/lista-zadataka-service.service';
import { ListItem } from '../../models/items';
import { LoaderServiceService } from '../services/loader-service.service';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.scss']
})
export class ListaComponent implements OnInit {
  private listaZadataka: ListItem[];
  @Output() emitForChips = new EventEmitter();

  constructor(private listaService: ListaZadatakaServiceService, private loader: LoaderServiceService) {
  }

  ngOnInit() {
    this.getListItems();
  }

  getListItems() {
    setTimeout(() => {
      this.listaService.getListItems().subscribe(
        listItems => {
          this.listaZadataka = listItems;
          this.emitForChips.emit(this.emitForChipsFunction(this.listaZadataka));
          this.loader.updateShowLoader(false);
        });
    }, 1500);
  }

  updateListItem(zadatak) {
    this.listaService.updateListItem(zadatak.id).subscribe(
      listItems => {
        this.listaZadataka = listItems;
        this.emitForChips.emit(this.emitForChipsFunction(this.listaZadataka));
      });
  }

  emitForChipsFunction(listaZadataka) {
    let reseni = 0;
    let nereseni = 0;
    listaZadataka.filter((zadatak) => {
      if (zadatak.isChecked) {
        nereseni++;
      } else {
        reseni++;
      }
    });
    return { reseni: reseni, nereseni: nereseni };
  }
}
