import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatToolbarModule,
  MatListModule,
  MatChipsModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatIconModule
} from '@angular/material';

import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ListaZadatakaServiceService } from './services/lista-zadataka-service.service';
import { ListaComponent } from './lista/lista.component';
import { ChipsComponentComponent } from './toolbar/chips-component/chips-component.component';
import { AplikacijaListaZadatakaComponent } from './aplikacija-lista-zadataka/aplikacija-lista-zadataka.component';
import { TextFormFieldComponent } from './utils-components/text-form-field/text-form-field.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {ToasterModule} from 'angular2-toaster';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    ListaComponent,
    ChipsComponentComponent,
    AplikacijaListaZadatakaComponent,
    TextFormFieldComponent,
    DashboardComponent
    ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatListModule,
    MatChipsModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    ToasterModule.forRoot()
  ],
  providers: [ListaZadatakaServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
