import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-chips-component',
  templateUrl: './chips-component.component.html',
  styleUrls: ['./chips-component.component.scss']
})
export class ChipsComponentComponent {
  @Input() reseniNereseniObjekat;
}
