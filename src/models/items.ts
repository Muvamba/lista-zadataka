
export class ListItem {
  constructor() {
    this.id = 0;
    this.description = '';
    this.isChecked = false;
  }
    public id: number;
    public description: String ;
    public isChecked: boolean;
  }
